<?php

return [
    'base_auth' => [
        'login' => env('REQUEST_LOGGER_LOGIN', 'login'),
        'password' => env('REQUEST_LOGGER_PASSWORD', 'password'),
    ],
    'timezone' => env('REQUEST_LOGGER_TIMEZONE', env('APP_TIMEZONE', config('app.timezone', 'UTC'))),
    'log_keep_days' => env('REQUEST_LOGGER_KEEP_DAYS', 14),
    'table_name' => 'request_logs',
    'hide_fields' => [
        'request' => [
            'headers' => [
                'authorization',
                'php-auth-user',
                'php-auth-pw',
            ],
            'content' => [
                'password',
                'token',
                'access_token',
            ],
        ],
        'response' => [
            'content' => [
                'password',
                'token',
                'access_token',
            ],
        ],
    ],
    'replacer_hidden_fields' => '|^_-|',
];
