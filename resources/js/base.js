let logTree = null;
document.addEventListener('DOMContentLoaded', function () {
    let modalViewData = document.getElementById('modalViewData')
    modalViewData.addEventListener('show.bs.modal', function (event) {
        let button = event.relatedTarget;
        let id = button.getAttribute('data-id');
        let treeWrapper = document.getElementById("tree-wrapper");
        treeWrapper.innerHTML = '';
        logTree = jsonTree.create({}, treeWrapper);
        let url = `/request-logs/${id}/show`;
        $.ajax({
            method: 'GET',
            url,
            dataType: 'json'
        }).done(function (jsonData) {
            logTree.loadData(jsonData)
        });
    });
});

$('#expandLogTree').click(function () {
    logTree.expand();
});

$('#collapseLogTree').click(function () {
    logTree.collapse();
});
