<?php

namespace Hryha\RequestLogger\Helpers;

use Hryha\RequestLogger\LogInfo;

class FileHelper
{
    /**
     * @param LogInfo $log_info
     * @return string
     */
    public function getFolderLogs(LogInfo $log_info): string
    {
        return "request-logs/"
            . "{$log_info->logger_start->format('Y')}-{$log_info->logger_start->format('m')}-{$log_info->logger_start->format('d')}/"
            . "{$log_info->logger_start->format('H')}h-{$log_info->logger_start->format('i')}m/";
    }

    /**
     * @param LogInfo $log_info
     * @return string
     */
    public function getLogFileName(LogInfo $log_info): string
    {
        $url = str_replace('/', '.', $log_info->request->path());
        return "{$url} # {$log_info->request->method()} # {$log_info->response->getStatusCode()} # {$log_info->logger_start->format('H.i.s.u')}.log";
    }

    /**
     * @param LogInfo $log_info
     * @return string
     */
    public function getLogPath(LogInfo $log_info): string
    {
        return $this->getFolderLogs($log_info) . $this->getLogFileName($log_info);
    }
}
