<?php

namespace Hryha\RequestLogger\Writers;

use Hryha\RequestLogger\LogInfo;

interface Writer
{
    public function write(LogInfo $log_info);
}
