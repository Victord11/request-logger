<?php

namespace Hryha\RequestLogger\Writers;

use Hryha\RequestLogger\Helpers\FileHelper;
use Hryha\RequestLogger\LogInfo;
use Hryha\RequestLogger\Models\RequestLog;
use Hryha\RequestLogger\Models\RequestLogFingerprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Throwable;

class DbWriter implements Writer
{
    /**
     * @var FileHelper $file_helper
     */
    protected $file_helper;

    /**
     * @param FileHelper $file_helper
     */
    public function __construct(FileHelper $file_helper)
    {
        $this->file_helper = $file_helper;
    }

    public function write(LogInfo $log_info)
    {
        try {
            DB::transaction(function () use ($log_info) {
                $fingerprint = RequestLogFingerprint::query()->firstOrCreate([
                    'fingerprint' => $log_info->fingerprint,
                ], [
                    'repeating' => 0,
                ]);

                RequestLog::query()->create([
                    'fingerprint_id' => $fingerprint->id,
                    'url' => Str::limit($log_info->request->fullUrl(), 250),
                    'method' => $log_info->request->method(),
                    'ip' => $log_info->request->getClientIp(),
                    'log_file' => $this->file_helper->getLogPath($log_info),
                    'date' => $log_info->logger_start->format('Y-m-d H:i:s.u'),
                    'response_status_code' => $log_info->response->getStatusCode(),
                    'duration_ms' => $log_info->duration_ms,
                    'memory' => $log_info->memory_usage,
                ]);

                $fingerprint->increment('repeating');
            });
        } catch (Throwable $e) {
            Log::error($e);
        }
    }
}
