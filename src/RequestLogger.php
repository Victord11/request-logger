<?php

namespace Hryha\RequestLogger;

use DateTime;
use DateTimeZone;
use Hryha\RequestLogger\Writers\LogWriter;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Log;
use Throwable;

class RequestLogger
{
    /**
     * @var LogInfo $log_info
     */
    public $log_info;

    /**
     * @var LogWriter $log_writer
     */
    protected $log_writer;

    /**
     * @param LogWriter $log_writer
     */
    public function __construct(LogWriter $log_writer)
    {
        $this->log_writer = $log_writer;
    }

    /**
     * @param Request $request
     * @param Response $response
     */
    public function save(Request $request, Response $response)
    {
        $this->request = $request;
        $this->response = $response;

        $this->log_info = new LogInfo(
            $request,
            $response,
            $this->getLoggerStart(),
            $this->getDuration(),
            $this->getMemoryUsage(),
            $this->getFingerprint()
        );

        $this->log_writer->write($this->log_info);
    }

    /**
     * @return float
     */
    protected function getStartTime(): float
    {
        $start_time = defined('LARAVEL_START') ? LARAVEL_START : (float)$this->request->server('REQUEST_TIME_FLOAT');
        if (!strpos($start_time, '.')) {
            $start_time .= '.0001';
        }
        return (float)$start_time;
    }

    /**
     * @return DateTime
     */
    protected function getLoggerStart(): DateTime
    {
        $start_time = $this->getStartTime();
        $logger_start = DateTime::createFromFormat('U.u', $start_time);

        try {
            $timezone = new DateTimeZone(config('request-logger.timezone'));
            $logger_start->setTimezone($timezone);
        } catch (Throwable $e) {
            Log::error($e);
        }

        return $logger_start;
    }

    /**
     * @return float
     */
    protected function getDuration(): float
    {
        $start_time = $this->getStartTime();
        return round((microtime(true) - $start_time) * 1000);
    }

    /**
     * @return float
     */
    protected function getMemoryUsage(): float
    {
        return round(memory_get_peak_usage(true) / 1024 / 1024, 1);
    }

    /**
     * @return string
     */
    public function getFingerprint(): string
    {
        return sha1(implode('|', [
            $this->request->fullUrl(),
            $this->request->getContent(),
        ]));
    }
}
