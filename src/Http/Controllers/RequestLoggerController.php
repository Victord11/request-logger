<?php

namespace Hryha\RequestLogger\Http\Controllers;

use Hryha\RequestLogger\Console\Commands\ClearRequestLogs;
use Hryha\RequestLogger\Models\RequestLogFingerprint;
use Hryha\RequestLogger\Services\ClearLogs;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Hryha\RequestLogger\Models\RequestLog;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\StreamedResponse;

class RequestLoggerController extends Controller
{
    /**
     * @param Request $request
     * @return Application|Factory|View
     */
    public function index(Request $request)
    {
        $total = RequestLog::query()
            ->joinFingerprint()
            ->adminFiltering($request)
            ->count();

        $logs = RequestLog::query()
            ->joinFingerprint()
            ->adminFiltering($request)
            ->paginate(50);

        return view('request-logs::index', [
            'logs' => $logs,
            'total' => $total,
            'request' => $request,
        ]);
    }

    /**
     * @param int $id
     * @return StreamedResponse
     */
    public function download(int $id): StreamedResponse
    {
        $request_log = RequestLog::query()->findOrFail($id);
        return Storage::download($request_log->log_file);
    }

    /**
     * @param int $id
     * @return string
     */
    public function show(int $id)
    {
        $request_log = RequestLog::query()->findOrFail($id);
        return Storage::get($request_log->log_file);
    }

    public function clearOldLogs(ClearLogs $clear_logs)
    {
        $clear_logs->clear();
        return redirect()->route('request-logs.index');
    }

    public function clearAllLogs(ClearLogs $clear_logs)
    {
        $clear_logs->clear(true);
        return redirect()->route('request-logs.index');
    }
}
