<?php

namespace Hryha\RequestLogger\Http\Middleware;

use Closure;

class BaseAuth
{
    /**
     * @param $request
     * @param Closure $next
     * @return mixed|void
     */
    public function handle($request, Closure $next)
    {
        $user = $request->server->get('PHP_AUTH_USER', '');
        $pass = $request->server->get('PHP_AUTH_PW', '');

        $is_authenticated = false;
        $has_supplied_credentials = !(empty($user) && empty($pass));

        if ($has_supplied_credentials
            && $user === config('request-logger.base_auth.login')
            && $pass === config('request-logger.base_auth.password')) {
            $is_authenticated = true;
        }

        if (!$is_authenticated) {
            header('HTTP/1.1 401 Authorization Required');
            header('WWW-Authenticate: Basic realm="Access denied"');
            exit;
        }

        return $next($request);
    }
}
