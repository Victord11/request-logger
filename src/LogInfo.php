<?php

namespace Hryha\RequestLogger;

use DateTime;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class LogInfo
{
    /** @var Request $request */
    public $request;

    /** @var Response $response */
    public $response;

    /** @var DateTime $logger_start */
    public $logger_start;

    /** @var integer $duration_ms */
    public $duration_ms;

    /** @var float $memory_usage */
    public $memory_usage;

    /** @var string $fingerprint */
    public $fingerprint;

    public function __construct(
        Request  $request,
        Response $response,
        DateTime $logger_start,
        int      $duration_ms,
        float    $memory_usage,
        string   $fingerprint)
    {
        $this->request = $request;
        $this->response = $response;
        $this->logger_start = $logger_start;
        $this->duration_ms = $duration_ms;
        $this->memory_usage = $memory_usage;
        $this->fingerprint = $fingerprint;
    }
}
