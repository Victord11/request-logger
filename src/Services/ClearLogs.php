<?php

namespace Hryha\RequestLogger\Services;

use Carbon\Carbon;
use DateTime;
use Hryha\RequestLogger\Models\RequestLog;
use Hryha\RequestLogger\Models\RequestLogFingerprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ClearLogs
{
    private $keep_days;

    public function __construct()
    {
        $this->keep_days = config('request-logger.log_keep_days');
    }

    /**
     * @param bool $all
     */
    public function clear(bool $all = false)
    {
        if ($all) {
            Storage::deleteDirectory('request-logs');
            DB::transaction(function () {
                RequestLog::query()->truncate();
                RequestLogFingerprint::query()->truncate();
            });
        } else {
            $now = Carbon::now()->endOfDay();
            $dirs = Storage::directories('request-logs');
            foreach ($dirs as $dir) {
                list($base_dir, $date_dir) = explode('/', $dir);
                if (DateTime::createFromFormat('Y-m-d', $date_dir) !== false) {
                    $date = Carbon::createFromFormat('Y-m-d', $date_dir)->endOfDay();
                    if ($date->diffInDays($now) >= $this->keep_days) {
                        Storage::deleteDirectory($dir);
                    }
                }
            }
            $last_date = Carbon::now()->subDays($this->keep_days)->endOfDay();
            DB::transaction(function () use ($last_date) {
                RequestLog::query()
                    ->where('date', '<=', $last_date)
                    ->delete();
                $delete_forever_ids = [];
                foreach (RequestLogFingerprint::query()->withCount('requestLogs')->cursor() as $fingerprint) {
                    if ($fingerprint->request_logs_count === 0) {
                        $delete_forever_ids[] = $fingerprint->id;
                    } else if ($fingerprint->request_logs_count !== $fingerprint->repeating) {
                        $fingerprint->update(['repeating' => $fingerprint->request_logs_count]);
                    }
                }
                RequestLogFingerprint::query()->whereIn('id', $delete_forever_ids)->delete();
            });
        }
    }
}
