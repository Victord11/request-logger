<?php

namespace Hryha\RequestLogger\Services;

use Symfony\Component\HttpFoundation\Response;

class ResponseFormatter
{
    /**
     * @var Concealer $concealer
     */
    protected $concealer;

    public function __construct(Concealer $concealer)
    {
        $this->concealer = $concealer;
    }

    /**
     * @param Response $response
     * @return array|false|string
     */
    public function getContent(Response $response)
    {
        if ($response instanceof \Illuminate\Http\JsonResponse) {
            $response_content = json_decode($response->getContent(), true);
            if (is_array($response_content)) {
                $response_content = $this->concealer->hide($response_content, config('request-logger.hide_fields.response.content'));
            }
        } else if ($response instanceof \Illuminate\Http\Response) {
            $response_content = $response->getContent();
        }

        return $response_content;
    }

    /**
     * @param Response $response
     * @return array
     */
    public function getHeaders(Response $response): array
    {
        $headers = $response->headers->all();
        foreach ($headers as $header => $values) {
            $headers[$header] = implode(',', $values);
        }
        return $headers;
    }
}
