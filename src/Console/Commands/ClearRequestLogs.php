<?php

namespace Hryha\RequestLogger\Console\Commands;

use Hryha\RequestLogger\Services\ClearLogs;
use Illuminate\Console\Command;

class ClearRequestLogs extends Command
{
    /**
     * @var string
     */
    protected $signature = 'request-logs:clear {--all}';

    /**
     * @var string
     */
    protected $description = 'Clear request logs';

    /**
     * @param ClearLogs $clear_logs
     */
    public function handle(ClearLogs $clear_logs)
    {
        $all = $this->option('all');
        $clear_logs->clear($all);
    }
}
