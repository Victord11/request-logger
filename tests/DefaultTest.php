<?php

namespace Hryha\RequestLogger\Tests;

use Hryha\RequestLogger\Helpers\FileHelper;
use Hryha\RequestLogger\Models\RequestLog;
use Hryha\RequestLogger\RequestLogger;
use Hryha\RequestLogger\RequestLoggerServiceProvider;
use Hryha\RequestLogger\Services\Concealer;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;
use Orchestra\Testbench\TestCase;

class DefaultTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();

        $this->loadLaravelMigrations(['--database' => 'testbench']);
        $this->loadMigrationsFrom([
            '--database' => 'testbench',
            '--path' => realpath(__DIR__ . '/../database/migrations'),
        ]);
    }

    protected function defineDatabaseMigrations()
    {
        $this->artisan('migrate', ['--database' => 'testbench'])->run();

        $this->beforeApplicationDestroyed(function () {
            $this->artisan('migrate:rollback', ['--database' => 'testbench'])->run();
        });
    }

    protected function getEnvironmentSetUp($app)
    {
        $app['config']->set('request-logger.table_name', 'request-logs');
        $app['config']->set('database.default', 'testbench');
        $app['config']->set('database.connections.testbench', [
            'driver' => 'mysql',
            'database' => 'request-logger',
            'host' => '127.0.0.1',
            'port' => '3306',
            'username' => 'homestead',
            'password' => 'secret',
        ]);
    }

    protected function getPackageProviders($app)
    {
        return [RequestLoggerServiceProvider::class];
    }

    public function testAuth()
    {
        $this->withoutMix();

        $login = 'login';
        $password = 'password';

        $this->app['config']->set('request-logger.base_auth', [
            'login' => $login,
            'password' => $password,
        ]);

        $server = [
            'PHP_AUTH_USER' => $login,
            'PHP_AUTH_PW' => $password
        ];

        $this->call('GET', '/request-logs', [], [], [], $server)
            ->assertStatus(200);
    }

    public function testHideData()
    {
        $data = [
            'login' => 'test',
            'password' => 'test',
            'data' => [
                [
                    'value',
                    'value2',
                ],
                [
                    'token' => 'token',
                ],
                [
                    'value3' => [
                        'value4'
                    ]
                ]
            ]
        ];
        $hide_fields = [
            'password',
            'token',
        ];
        $data_replaced = [
            'login' => 'test',
            'password' => $this->app->config['request-logger']['replacer_hidden_fields'],
            'data' => [
                [
                    'value',
                    'value2',
                ],
                [
                    'token' => $this->app->config['request-logger']['replacer_hidden_fields'],
                ],
                [
                    'value3' => [
                        'value4'
                    ]
                ]
            ]
        ];

        $concealer = new Concealer();
        $result = $concealer->hide($data, $hide_fields);
        $this->assertEquals($data_replaced, $result);
    }

    public function testWriteLog()
    {
        $request = Request::create('/test', 'POST', [], [], [], [], 'test');
        $response = new Response('test', 200);

        $file_helper = $this->app->make(FileHelper::class);
        $request_logger = $this->app->make(RequestLogger::class);
        $request_logger->save($request, $response);

        $this->assertEquals(1, RequestLog::query()->count());
        $this->assertEquals(true, Storage::exists($file_helper->getLogPath($request_logger->log_info)));
    }
}
